package Property;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Flat {
    public final int number;
    public final int area;
    public List<Person> owners;

    public Flat(int number, int area, List<Person> owners) {
        this.number = number;
        this.area = area;
        this.owners = new ArrayList<>(owners);
    }

    @Override
    public String toString() {
        return "Flat{" +
                "number=" + number +
                ", area=" + area +
                ", owners=" + owners +
                '}';
    }

    public void setOwners(List<Person> owners) {
        this.owners = new ArrayList<>(owners);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Flat)) return false;
        Flat flat = (Flat) o;
        return number == flat.number &&
                area == flat.area &&
                owners.equals(flat.owners);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, area, owners);
    }
}
