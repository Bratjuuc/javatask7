package Property;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private final String cadastrialNumber;
    private final String adress;
    private final Person responsible;
    private final List<Flat> flats;

    @Override
    public String toString() {
        return "House{" +
                "cadastrialNumber='" + cadastrialNumber + '\'' +
                ", adress='" + adress + '\'' +
                ", responsible=" + responsible +
                ", flats=" + flats +
                '}';
    }

    public House(String cadastrialNumber, String adress, Person responsible, List<Flat> flats) {
        this.cadastrialNumber = cadastrialNumber;
        this.adress = adress;
        this.responsible = responsible;
        this.flats = flats;
    }

    public String getCadastrialNumber() {
        return cadastrialNumber;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public Person getResponsible() {
        return responsible;
    }

    public String getAdress() {
        return adress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof House)) return false;
        House house = (House) o;
        return cadastrialNumber.equals(house.cadastrialNumber) &&
                adress.equals(house.adress) &&
                responsible.equals(house.responsible) &&
                flats.equals(house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastrialNumber, adress, responsible, flats);
    }
}
