import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.CharBuffer;
import java.util.*;

public class FirstTasks {
    @NotNull
    static void writeInts(Integer[] array,@NotNull OutputStream oStr) throws IOException {
        DataOutputStream oStream = new DataOutputStream(oStr);
        for (Integer i: array){
            oStream.writeInt(i);
        }
    }
    @NotNull
    static Integer[] readIntsFromStream(Integer[] result,@NotNull InputStream iStr) throws IOException {
        DataInputStream iStream = new DataInputStream(iStr);
        for (int i = 0; i < result.length; i++){
            result[i] = iStream.readInt();
        }
        return result;
    }
    @NotNull
    static void writeIntsWithSymbols(Integer[] array,@NotNull Writer oStr) throws IOException {
        //Writer oStream = new BufferedWriter(oStr); -- Всё ломает
        for (Integer i: array){
            oStr.append(" ").append(i.toString());
        }
    }
    @NotNull
    static Integer[] readIntsFromStreamWithSymbols(Integer[] result,@NotNull Reader iStr) throws IOException {
        int t, i;
        boolean flag = true;

        if ((t = iStr.read()) == -1){
            throw new RuntimeException("Empty File");
        }
        else if ( (char) t != ' ') {

        }

        for (i = 0; i < result.length && flag; i++){
            StringBuilder str = new StringBuilder(32);
            while (flag) {
                if ((t = iStr.read()) == -1 ){
                    result[i] = Integer.parseInt(str.toString());
                    flag = false;
                    break;
                }
                if ( (char) t != ' ') {
                    str.append((char) t);
                }
                else {
                    break;
                }
            }
            result[i] = Integer.parseInt(str.toString());
        }
        if (i != result.length){
            throw new RuntimeException("File is too short");
        }
        return result;
    }

    static Integer[] readFromRandomAccessFile (@NotNull RandomAccessFile file, int n, int offset) throws IOException {
        if (n < 0 ){
            throw new IllegalArgumentException();
        }
        file.seek(offset);
        Integer[] result = new Integer[n];
        for (int i = 0; i < n; i++){
            result[i] = Integer.parseInt(file.readLine());
        }
        return result;
    }

    static public List<File> getListOfFilesWithGivenExtension(String catalog,String extension){
        File currentCatalog = new File(catalog);
        List<File> result = new LinkedList<>();
        File [] fileArray = currentCatalog.listFiles();
        if (fileArray == null){
            return new LinkedList<>();
        }

        for (File f: fileArray){
            /*if (f.isDirectory()){
                result.addAll(getListOfFilesWithGivenExtension(catalog + "/" + f.getName() ,extension));
            }*/
            if (f.isFile() && f.getName().endsWith("." + extension)){
                result.add(f);
            }
        }
        return result;
    }

    /*
    @NotNull
    public static CharArrayWriter writeToCharStream(@NotNull int[] arr) {
        CharArrayWriter outStream = new CharArrayWriter();
        for (int item : arr)
            outStream.append(Integer.toString(item)).append(" ");
        outStream.flush();
        return outStream;
    }

    public static void readFromCharStream(CharArrayReader stream, @NotNull int[] arr) {
        Scanner scanner = new Scanner(stream);
        for (int i = 0; i < arr.length; i++) {
            if (!scanner.hasNext())
                return;
            arr[i] = Integer.parseInt(scanner.next());
        }
    }

    @NotNull
    public static ByteArrayOutputStream writeToBinaryStream(@NotNull int[] arr) throws IOException {
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
            for (int item : arr)
                outStream.write(item);
            outStream.flush();
            return outStream;
        }
    }

    public static void readFromBinaryStream(ByteArrayInputStream stream, @NotNull int[] arr) {
        int buf;
        for (int i = 0; i < arr.length; i++) {
            if ((buf = stream.read()) == -1)
                return;
            arr[i] = buf;
        }
    }
    */

}
