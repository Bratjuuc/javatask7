import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class InputOutput {

    @NotNull
    public static ByteArrayOutputStream writeToBinaryStream(@NotNull int[] arr) throws IOException {
        try (ByteArrayOutputStream outStream = new ByteArrayOutputStream()) {
            for (int item : arr)
                outStream.write(item);
            outStream.flush();
            return outStream;
        }
    }

    public static void readFromBinaryStream(ByteArrayInputStream stream, @NotNull int[] arr) {
        int buf;
        for (int i = 0; i < arr.length; i++) {
            if ((buf = stream.read()) == -1)
                return;
            arr[i] = buf;
        }
    }

    @NotNull
    public static CharArrayWriter writeToCharStream(@NotNull int[] arr) {
        CharArrayWriter outStream = new CharArrayWriter();
        for (int item : arr)
            outStream.append(Integer.toString(item)).append(" ");
        outStream.flush();
        return outStream;
    }

    public static void readFromCharStream(CharArrayReader stream, @NotNull int[] arr) {
        Scanner scanner = new Scanner(stream);
        for (int i = 0; i < arr.length; i++) {
            if (!scanner.hasNext())
                return;
            arr[i] = Integer.parseInt(scanner.next());
        }
    }

    static public List<File> filesWithExtension(String catalog, String extension) {
        File curCatalog = new File(catalog);
        File[] listFile = curCatalog.listFiles();
        if (listFile == null)
            return new ArrayList<>();
        List<File> out = new ArrayList<>(listFile.length);
        for (File f : listFile) {
            if (f.isFile() && f.getName().endsWith(extension))
                out.add(f);
        }
        return out;
    }

    static public List<File> regExMatchFiles(String catalog, String regEx) {
        File curCatalog = new File(catalog);
        File[] listFile = curCatalog.listFiles();
        if (listFile == null)
            throw new IllegalArgumentException("Передан не каталог!");
        List<File> out = new ArrayList<>(listFile.length);
        // Заходим в рекурсию
        addFilesInDir(curCatalog, regEx, out);
        return out;
    }

    static private void addFilesInDir(@NotNull File curCatalog, String regEx, List<File> list) {
        File[] listFile = curCatalog.listFiles();
        for (File f : listFile) {
            if (Pattern.matches(regEx, f.getName()))
                list.add(f);
            if (f.isDirectory())
                addFilesInDir(f, regEx, list);
        }
    }
}