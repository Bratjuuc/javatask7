import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.testng.*;
import org.testng.annotations.DataProvider;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class FirstTasksTest {

    @DataProvider(name = "arrays")
    public Object[][] testArrays() {
        return new Object[][]{
                {},
                {1},
                {1,2,3,4,5,6},
                {12345},
                {123456,-2222}
        };
    }
    //@org.junit.jupiter.api.Test
    //@Test(dataProvider = "arrays")
    public void writeInts() throws IOException {
        Integer [] testInts = {123456,-2222};
        Integer[] result = new Integer[testInts.length];
        OutputStream oStream = new FileOutputStream("writeInts.txt");
        FirstTasks.writeInts(testInts, oStream);
        oStream.close();

        FileInputStream fis =  new FileInputStream("writeInts.txt");
        FirstTasks.readIntsFromStream(result,fis);
        Deque<Integer> actual = new ArrayDeque<>(Arrays.asList(result));

        for (Integer i: testInts){
            Assert.assertEquals(i, actual.removeFirst());
        }
        fis.close();

    }

    @org.junit.jupiter.api.Test
    void readIntsFromStreamTest() throws IOException {
        Integer [] testInts = {1,2,3,4,5,6};
        Integer[] result = new Integer[testInts.length];

        DataOutputStream dos = new DataOutputStream(new FileOutputStream("writer.txt"));
        for(Integer i: testInts) {
            dos.writeInt(i);
        }
        dos.close();

        FileInputStream fis =  new FileInputStream("writer.txt");
        FirstTasks.readIntsFromStream(result,fis);
        Deque<Integer> actual = new ArrayDeque<>(Arrays.asList(result));

        for (Integer i: testInts){
            Assert.assertEquals(i, actual.removeFirst());
        }
        fis.close();
    }

    @org.junit.jupiter.api.Test
    void writeIntsWithSymbolsTest() throws IOException {
        Integer [] testInts = {1,2,3,4,5,6,123};
        Integer [] result = new Integer[testInts.length];
        Writer oStream = new FileWriter("writeIntsWS.txt");
        FirstTasks.writeIntsWithSymbols(testInts, oStream);
        oStream.close();

        System.out.println(Files.readString(Paths.get("writeIntsWS.txt")));
        Assert.assertEquals(" 1 2 3 4 5 6 123", Files.readString(Paths.get("writeIntsWS.txt")));
    }

    @org.junit.jupiter.api.Test
    void readIntsFromStreamWithSymbols() throws IOException {
        Integer [] testInts = {1,2,3,4,5,6,123, -333};
        Integer [] result = new Integer[testInts.length];

        FileWriter dos = (new FileWriter("writer.txt"));
        for(Integer i: testInts) {
            dos.write(" " + i.toString());
        }
        dos.close();

        FileReader fis =  new FileReader("writer.txt");
        FirstTasks.readIntsFromStreamWithSymbols(result,fis);
        Deque<Integer> actual = new ArrayDeque<>(Arrays.asList(result));

        for (Integer i: testInts){
            Assert.assertEquals(i, actual.removeFirst());
        }
        fis.close();
    }

    @Test
    void testReadFromRandomAccessFile() throws IOException {
        FileWriter file = new FileWriter("file.txt");
        file.write("0\n0\n0\n0\n12\n33");
        file.close();

        RandomAccessFile randomAccessFile = new RandomAccessFile("file.txt", "r");
        Assert.assertEquals(new Integer[]{12, 33}, FirstTasks.readFromRandomAccessFile(randomAccessFile, 2, 8));

    }

    @Test
    void getListOfFilesTest() throws IOException {
        File [] files = {new File("TestData/file1.txt"),
                new File("TestData/file2.mp3"),
                new File("TestData/file3.txt"),
                new File("TestData/sampleFolder"),
                new File("TestData/emptyFolder")};
        for (int i = 0; i < 3; i++){
            files[i].createNewFile();
        }
        files[3].mkdir();
        files[4].mkdir();

        File internalFile = new File("TestData/sampleFolder/internalFile.txt");
        internalFile.createNewFile();

        Deque expected = mock(Deque.class);
        Mockito.when(expected.removeLast()).thenReturn("file1.txt", "file3.txt");

        for (File str : FirstTasks.getListOfFilesWithGivenExtension("TestData" , "txt")/*FirstTasks.getListOfFilesWithGivenExtension("", "txt")*/ ) {
            Assert.assertEquals(expected.removeLast(), str.getName());
        }
    }

    @Test
    void regExMatchFilesTest() throws IOException {
        File folder = new File("RegExTest");
        folder.mkdir();

        File[] files = {new File("RegExTest/q2.txt"),
                new File("RegExTest/q.txt"),
                new File("RegExTest/q.hs"),
                new File("RegExTest/qq.txt"),
                new File("RegExTest/qqqq.txt"),
                new File("RegExTest/.txt"),
                new File("RegExTest/.d"),
                new File("RegExTest/.ddd"),
                new File("RegExTest/1.d"),
                new File("RegExTest/1.ddd"),
                new File("RegExTest/22.135")
        };

        for (File f: files){
            f.createNewFile();
        }
        File innerFolder = new File("RegExTest/innerFolder");
        innerFolder.mkdir();

        Deque expected1 = mock(Deque.class);
        Mockito.when(expected1.removeLast()).thenReturn(files[1].getName(), files[0].getName());
        Deque expected2 = mock(Deque.class);
        Mockito.when(expected1.removeLast()).thenReturn(files[1].getName(), files[0].getName());
        Deque expected3 = mock(Deque.class);
        Mockito.when(expected1.removeLast()).thenReturn(files[1].getName(), files[0].getName());

        for (File str : InputOutput.regExMatchFiles(folder.getName(), "q*/.txt")) {
            //Assert.fail("s");
            Assert.assertEquals(expected1.removeLast(), str.getName());
        }
        for (File str : InputOutput.regExMatchFiles(folder.getPath() , ".?/.d*")) {
            //Assert.fail("s");
            Assert.assertEquals(expected2.removeLast(), str.getName());
        }
        for (File str : InputOutput.regExMatchFiles(folder.getPath() , "/.[135]*")) {
            //Assert.fail("s");
            Assert.assertEquals(expected3.removeLast(), str.getName());
        }
    }




    /*@Test
    void writeToCharStreamTest() throws IOException {
        int [] testInts = {1,2,3,4,5,6};
        Integer [] result = new Integer[testInts.length];
        Writer oStream = FirstTasks.writeToCharStream(testInts);


    }

    @Test
    void readFromCharStreamTest() throws IOException {
        int [] testInts = {1,2,3,4,5,6,123,-333};
        int [] result = new int [testInts.length];
        StringBuilder str = new StringBuilder("");

        for(Integer i: testInts) {
            str.append(" ").append(i.toString());
        }

        char[] buf = str.toString().toCharArray();
        CharArrayReader fis = new CharArrayReader(buf);

        FirstTasks.readFromCharStream(fis, result);

        Deque<Integer> actual = new ArrayDeque<Integer>();
        for (int i: result){
            actual.addLast(i);
        }
        //Deque<Integer> actual = Mockito.mock(Deque.class);
        //Mockito.when(actual.removeFirst()).thenReturn(testInts);
        for (Integer i: testInts){
            Assert.assertEquals(i, actual.removeFirst());
        }
        fis.close();
    }

    @Test
    void writeToBinaryStream() {
    }

    @Test
    void readFromBinaryStream() throws IOException {
        int [] testInts = {1,2,3,4,5,6,123,-333};
        int [] result = new int [testInts.length];
        StringBuilder str = new StringBuilder("");

        DataOutputStream dos = new DataOutputStream(new FileOutputStream("writer.txt"));
        for(Integer i: testInts) {
            dos.writeInt(i);
        }
        dos.close();

        FileInputStream fs = new FileInputStream("writer.txt");
        ByteArrayInputStream fis = new ByteArrayInputStream(fs.readAllBytes());
        FirstTasks.readFromBinaryStream(fis, result);

        Deque<Integer> actual = new ArrayDeque<Integer>();
        for (int i: result){
            actual.addLast(i);
            System.out.println(i);
        }
        //Deque<Integer> actual = Mockito.mock(Deque.class);
        //Mockito.when(actual.removeFirst()).thenReturn(testInts);
        for (Integer i: testInts){
            Assert.assertEquals(i, actual.removeFirst());
        }
        fs.close();
        fis.close();
    }*/
}